# Project splitted into four

- https://github.com/kilianpaquier/exithandler
- https://github.com/kilianpaquier/filesystem
- https://github.com/kilianpaquier/pooling
- https://github.com/kilianpaquier/testlogrus

# gtl <!-- omit in toc -->

- [How to use ?](#how-to-use-)
- [Packages](#packages)
  - [exithandler](#exithandler)
  - [filesystem](#filesystem)
  - [testlogrus](#testlogrus)
  - [pooling](#pooling)

## How to use ?

```sh
go get gitlab.com/gotools-land/gtl@latest
```

## Packages

### exithandler

The exithandler package exposes two useful functions to handle program terminations:

The first one is `Handle` which will blocked on SIGINT and SIGTERM signals until one of those are sent and then executes the provided function.

```go
func main() {
  // some things to be defined

  go exithandler.Handle(ctx, func(context.Context) {
    // some things to close or execute when the program terminates
  })
}
```

The second one is `HandleFunc` which does the exact same thing, the only difference is that it returns the function which will wait and does not wait directly (as provided in below example).

```go
func main() {
  // some things to be defined

  exithandler := exithandler.HandleFunc(ctx, func(context.Context) {
    // some things to close or execute when the program terminates
  })

  // other things to do

  exithandler()
}
```

### filesystem

The filesystem package exposes some useful function around files, for instance a simple function as `func Exists(src) bool` to verify a file existence easily.

It also exposes `CopyFile(src, dst) error` and `CopyFileWithPerm(src, dst, perm) error` which copy a given src file to dst with either specific permissions or not.

It also exposes `CopyDir(srcdir, destdir)` to copy a full directory at another place. The destination directory will be created if it doesn't already.

The package also exposes some constants around permissions.

### testlogrus

The testlogrus packages exposes two functions to use during testing. When calling `Logs` function, the logs are reset, as such if multiple verification are to be done, you should first affect `Logs` result to a variable.

```go
func TestSome(t *testing.T) {
  t.Run("error_test", func(t *testing.T) {
    // Arrange
    testlogrus.CatchLogs()

    // Act
    // call some functions that log with logrus

    // Assert
    logs := testlogrus.Logs()
    // assert some things around function expected logs
  })
}
```

### pooling

The pooling package allows one to dispatch an infinite number of functions to be executed in parallel while still limiting the number of routines.

For that, pooling package takes advantage of ants pool library. A pooling Pooler can have multiple pools (with builder SetSizes) to dispatch sub functions into different pools of routines.

When sending a function into the pooler (with the appropriate channel), this function can itself send other functions into the pooler. It allows one to "split" functions executions (like iterating over a slice and each element handled in parallel).

```go
func main() {
  log := logrus.WithContext(context.Background())

  pooler, err := pooling.NewPoolerBuilder().
    SetSizes(10, 500, ...). // each size will initialize a pool with given size
    SetOptions(ants.WithLogger(log)).
    Build()
  if err != nil {
    panic(err)
  }
  defer pooler.Close()

  input := ReadFrom()

  // Read function is blocking until input is closed
  // and all running routines have ended
  pooler.Read(input)
}

func ReadFrom() <-chan pooling.PoolerFunc {
  input := make(chan pooling.PoolerFunc)

  go func() {
    // close input to stop blocking function Read once all elements are sent to input
    defer close(input)

    // do something populating input channel
    for i := range 100 {
      input <- HandleInt(i)
    }
  }()

  return input
}

func HandleInt(i int) pooling.PoolerFunc {
  return func(funcs chan<- pooling.PoolerFunc) {
    // you may handle the integer whichever you want
    // funcs channel is present to dispatch again some elements into a channel handled by the pooler
  }
}
```

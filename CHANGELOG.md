## [2.0.2](https://gitlab.com/gotools-land/gtl/compare/v2.0.1...v2.0.2) (2024-02-19)


### Bug Fixes

* **filesystem:** reduce AssertEqualDir complexity in tests package ([e84e944](https://gitlab.com/gotools-land/gtl/commit/e84e944dcee790780320c2c06aaa04d878563a8b))
* **lint:** some issues on unused parameters ([ff0cfbb](https://gitlab.com/gotools-land/gtl/commit/ff0cfbb64dfec4de4d81e6e7da413c79d7e91d26))
* **pooling:** simplify and improve testing ([fd95e4c](https://gitlab.com/gotools-land/gtl/commit/fd95e4c1a7ad67a99c5ebc365e3fd5a818ac8361))
* **pooling:** unstable unit test ([46f0874](https://gitlab.com/gotools-land/gtl/commit/46f0874af6ec2a0b8d643ad861b4e48ce34d379f))


### Documentation

* **filesystem:** add doc to filterDiffs function in tests package ([41aa98e](https://gitlab.com/gotools-land/gtl/commit/41aa98edc5c8fa6570b3df80119660b83cafa1f1))

## [2.0.1](https://gitlab.com/gotools-land/gtl/compare/v2.0.0...v2.0.1) (2024-02-19)


### Bug Fixes

* **lint:** preallocate errs before ranging over defined size of elements ([72f73cf](https://gitlab.com/gotools-land/gtl/commit/72f73cf66054112cf3696f5faea438b9d386802c))


### Chores

* remove samber/lo usages in favor of good old school for loops (only because it's a shared library, the less imports there is the better it is) ([61d787d](https://gitlab.com/gotools-land/gtl/commit/61d787dfee1bc385b3c7596e9f8a13fb1766a055))

## [2.0.0](https://gitlab.com/gotools-land/gtl/compare/v1.1.0...v2.0.0) (2024-02-19)


### ⚠ BREAKING CHANGES

* **testutils:** testutils package doesn't exist anymore

### Chores

* **v2:** prepare migration with go imports ([c1b9a0e](https://gitlab.com/gotools-land/gtl/commit/c1b9a0e460488048fe4721ec6fe1598df31008b6))


### Code Refactoring

* **testutils:** remove package ([1636496](https://gitlab.com/gotools-land/gtl/commit/163649689dcc02a2c557bb1f8495ea6801373830))

## [1.1.0](https://gitlab.com/gotools-land/gtl/compare/v1.0.2...v1.1.0) (2024-02-19)


### Chores

* **golangci:** remove fieldalignment lint ([9a0286d](https://gitlab.com/gotools-land/gtl/commit/9a0286d640a45e39d6645dd1678b41d2ecf733a3))
* update make generate command ([0fe3452](https://gitlab.com/gotools-land/gtl/commit/0fe34528439ec5425318e4b7e010e68922c2d32b))


### Code Refactoring

* **pooling:** simplify readAt function ([71964ab](https://gitlab.com/gotools-land/gtl/commit/71964ab9fcd8df304ef8f4f695f29e651c7425cd))

## [1.0.2](https://gitlab.com/gotools-land/gtl/compare/v1.0.1...v1.0.2) (2024-02-17)


### Bug Fixes

* **exithandler:** unstable test ([043f56b](https://gitlab.com/gotools-land/gtl/commit/043f56b5b4ade52a111f8740416e9f53da3c403c))


### Documentation

* remove examples section in package doc to avoid unnecessary table content in pkg dev go ([137a808](https://gitlab.com/gotools-land/gtl/commit/137a808017d709fcabcfabe303a879496d8ea956))

## [1.0.1](https://gitlab.com/gotools-land/gtl/compare/v1.0.0...v1.0.1) (2024-02-15)


### Chores

* **gomod:** retract v1.0.0 (invalid release) ([60a65d3](https://gitlab.com/gotools-land/gtl/commit/60a65d3a5c3433fba0136e13e636a9add4474350))

## 1.0.0 (2024-02-15)


### ⚠ BREAKING CHANGES

* **yaml:** no more yaml functions
* **dir:** renamed dir package to filesystem
* **yaml:** yaml package was renamed

### Features

* add FS interface to handle different filesystems ([230b000](https://gitlab.com/gotools-land/gtl/commit/230b000a679b22941bc8a1b8c86dde544b532035))
* add gob simple encoding and decoding alongside zstd simple compressing and uncompressing ([dcd3fdc](https://gitlab.com/gotools-land/gtl/commit/dcd3fdc69922142649f60642e713b59fd0a52ad5))
* add home to constants ([e346af8](https://gitlab.com/gotools-land/gtl/commit/e346af841278028cc17352033f8cb694ce65dcc2))
* add httpcache ([d491043](https://gitlab.com/gotools-land/gtl/commit/d491043d469082180464eb80a2e2874f7bde1b2e))
* add option WithFS to filesystem package ([bb5a5c7](https://gitlab.com/gotools-land/gtl/commit/bb5a5c7164545561d4ff138a5583e8baf28e39b1))
* add ptr Copy func ([179f87f](https://gitlab.com/gotools-land/gtl/commit/179f87fa81c0e47921eaeb14d9b506943f82bcdd))
* add WalkDir to filesystem package because of unsatisfying filepath function ([faea2de](https://gitlab.com/gotools-land/gtl/commit/faea2def3dd2b2c58b827e181f70b7db89299b12))
* **cache:** add package to handle in memory and redis ([27ae01e](https://gitlab.com/gotools-land/gtl/commit/27ae01e0de2549ab506b032c7bc79df80f7feffd))
* **cache:** add zstd compression ([1e7cd61](https://gitlab.com/gotools-land/gtl/commit/1e7cd6110d409d83d9393ee3533746faceed9ba8))
* **cache:** use zstd and gob to store cache values ([41282bb](https://gitlab.com/gotools-land/gtl/commit/41282bb5193ad1efc7effdbd0b17a6b7e61fdad7))
* **constants:** add GOCMD and GOMAIN ([1dd6655](https://gitlab.com/gotools-land/gtl/commit/1dd6655fc6122dd2d3b63e0e7ebf37b5180c9ee6))
* **exithandler:** add signal package listener ([d98653c](https://gitlab.com/gotools-land/gtl/commit/d98653ce901e756c40dc920317fc4c2a44702ece))
* **exithandler:** allow override of signals and add delegated function for later execution ([a9d6151](https://gitlab.com/gotools-land/gtl/commit/a9d6151d5c29e3eb5562cf49444c56e13b534a48))
* **filesystem:** add Exists function ([753be5b](https://gitlab.com/gotools-land/gtl/commit/753be5b8753c964671e838a56d37dda4def03ab2))
* init project with some tools ([be98326](https://gitlab.com/gotools-land/gtl/commit/be98326cb05aeabdd7485c901a8f60ce26c4a46e))
* **pooling:** add package to channel oriented dispatch of executions ([130a019](https://gitlab.com/gotools-land/gtl/commit/130a0193de848bbf28a6d007a0d713b4f2b30034))
* **ptr:** add pointer simple operations ([7f17743](https://gitlab.com/gotools-land/gtl/commit/7f1774319ac22c21bbea59819ebb0689e80885c6))
* remove dir folder in favor of manual functions ([dd42693](https://gitlab.com/gotools-land/gtl/commit/dd42693c7fd5444f5b283dad3918b7070f8b57c6))
* remove ReadDir and CopyDir as there's WalkDir in filepath ([2f03a92](https://gitlab.com/gotools-land/gtl/commit/2f03a925de92ab7d3b38eb9ba15e2283e0cd1ac8))
* remove useless constants package ([6961d87](https://gitlab.com/gotools-land/gtl/commit/6961d8708af08b7809c9c58a51c74244e28706da))
* **taskhandler:** handle locked tasks ([3da7a5b](https://gitlab.com/gotools-land/gtl/commit/3da7a5bf881b4505b5afbe2bbcdfa3c4a03c808e))
* **tasks:** add taskhandler concept ([c306191](https://gitlab.com/gotools-land/gtl/commit/c30619131229462e63f41fbbf98beddc9719280b))
* **testing:** add testlogrus package to read logs in unit tests ([16168d9](https://gitlab.com/gotools-land/gtl/commit/16168d9328160866f443150a9294a0bc96cfbbc7))
* **testutils:** add AssertEqualDir function to compare directories ([ca38fba](https://gitlab.com/gotools-land/gtl/commit/ca38fba01c1c9acb11da6e84d74712a2512a71d8))
* **yaml:** add read wrap function ([3493769](https://gitlab.com/gotools-land/gtl/commit/3493769f22d5931ba59beb6230174b142326d094))
* **yaml:** remove functions ([719464e](https://gitlab.com/gotools-land/gtl/commit/719464eef1994136cea96baf86fc1a092b492dac))
* **zstd:** add simple compressor and decompressor ([e9ad186](https://gitlab.com/gotools-land/gtl/commit/e9ad186cc09e575cd19e6329e7eb38aece029f92))
* **zstd:** remove useless sublayer ([7187eb4](https://gitlab.com/gotools-land/gtl/commit/7187eb440216ef603ab7e76e426d2963f1b73ce6))


### Bug Fixes

* add missing dependencies for AssertEqualDir ([92fe550](https://gitlab.com/gotools-land/gtl/commit/92fe550b55ee0334e00e641ec629dec78ae555af))
* add tests for coverage and missed cases ([8c33926](https://gitlab.com/gotools-land/gtl/commit/8c33926640a3233db4e5a1cc9f6e58eed97f26e2))
* **dir:** nil err in case destdir doesn't exist ([6be7d66](https://gitlab.com/gotools-land/gtl/commit/6be7d663b4794db7ac863650bf20d1abf8f4104a))
* **go.mod:** missing v2 for release ([1f8a995](https://gitlab.com/gotools-land/gtl/commit/1f8a9950d4d75aeb3a24a409b6dfc41fc3058ac6))
* handle nil opt in EqualDir ([b733ac3](https://gitlab.com/gotools-land/gtl/commit/b733ac3f9b91ee0a76b7ab34e65edb44850587c9))
* lint issues ([d138175](https://gitlab.com/gotools-land/gtl/commit/d138175f4bca7ac5f9bd30037a3d6a3ffbaa3a63))
* linting issues ([bcddcc1](https://gitlab.com/gotools-land/gtl/commit/bcddcc1e5c522561efe62a27c501dfdbf92e5fe7))
* linting issues ([ce08abf](https://gitlab.com/gotools-land/gtl/commit/ce08abf417cd9b674577136cc3e3726537df3bc2))
* linting issues ([6332f7e](https://gitlab.com/gotools-land/gtl/commit/6332f7e65d4865a60bdfc3fdb6292b6b64e1ff71))
* **lint:** linting issues and coverage of exithandler ([9bdc937](https://gitlab.com/gotools-land/gtl/commit/9bdc937f58b1986de31cd59a10c1c8b57b18ce21))
* **testlogrus:** panic if Logs is called without CatchLogs ([4d7d829](https://gitlab.com/gotools-land/gtl/commit/4d7d829563585aed23ed3d4d038c08c8b1c43de8))
* **tests:** changed time.Sleep duration and remove sigint for context with timeout ([7c5dfdc](https://gitlab.com/gotools-land/gtl/commit/7c5dfdc3b51b41d9328cd15424d801681ce7f83e))
* **UT:** renamed expected error strings ([36c7af0](https://gitlab.com/gotools-land/gtl/commit/36c7af01352d988776867741ddca719d69392626))
* **yaml:** change yaml rights after merge to 0644 ([7812df4](https://gitlab.com/gotools-land/gtl/commit/7812df4c79b8d1bd31dd7eeafc2dfdad201b7582))


### Reverts

* change back package to v0 ([2faffcf](https://gitlab.com/gotools-land/gtl/commit/2faffcfd3456b523ee453c220c9b023b684a8e1e))
* **logging:** remove logger package ([1d97913](https://gitlab.com/gotools-land/gtl/commit/1d9791365913f54414bac3ef8717ed3a2d8c21c7))
* remove cache, httpcache and other useless go packages ([fa05215](https://gitlab.com/gotools-land/gtl/commit/fa052155c6ae60d010fbeb4800b548d51185d48b))


### Documentation

* add minimal README with exposed functionalities ([3aa7efb](https://gitlab.com/gotools-land/gtl/commit/3aa7efbec789a617bd2974cba0b195077721734e))
* add package comment to existing packages ([7b0d1e6](https://gitlab.com/gotools-land/gtl/commit/7b0d1e6591eff4760e91bd7a796055cf64b78143))
* update README.md following constants package refactoring ([bd09ed3](https://gitlab.com/gotools-land/gtl/commit/bd09ed32061f54117803118a6cd155660c423e8f))
* **validator:** add FormatErrors doc ([0782f30](https://gitlab.com/gotools-land/gtl/commit/0782f3017234d9c5867d00dd7908d07d48c96aa9))


### Chores

* add **/tests/** to go coverage exclusions ([2415c57](https://gitlab.com/gotools-land/gtl/commit/2415c57c1f536948fd3fa9ef84229d5fb0973e21))
* add common files rights to filesystem package ([69d9c3b](https://gitlab.com/gotools-land/gtl/commit/69d9c3bf6b6b83cef01e2b91ce6281147c312ef4))
* add constants for darwin based on linux ([f165ac2](https://gitlab.com/gotools-land/gtl/commit/f165ac2f3253d54dc56a4e4e1a4e5c3e7f8084eb))
* **ci:** regenerate file ([3cbe765](https://gitlab.com/gotools-land/gtl/commit/3cbe765a1796b48a0b489cafa1d3d553cf94b67d))
* **constants:** edited Pwd doc ([da61207](https://gitlab.com/gotools-land/gtl/commit/da61207feff5e30ebf3e0a5991fcc040fe721474))
* **constants:** rework to only expose them as readonly ([f924256](https://gitlab.com/gotools-land/gtl/commit/f924256aaca7df0b0dbe5bc7d3af5b1f94935856))
* **craft:** generate layout ([d46b622](https://gitlab.com/gotools-land/gtl/commit/d46b6223f2360f3c22195f6013768c1329cc7a79))
* **craft:** regenerate project ([a4c5b75](https://gitlab.com/gotools-land/gtl/commit/a4c5b757463cd35b7423936e12ae28809f1fcdd8))
* **craft:** update generation ([b95c600](https://gitlab.com/gotools-land/gtl/commit/b95c6005e39fe59fecffe8a8c0b8aadfae02094e))
* **craft:** update layout ([557bc6e](https://gitlab.com/gotools-land/gtl/commit/557bc6e3ef9d2116f116acb39bc6245d3088cb16))
* **craft:** update sonar.properties ([0f97d07](https://gitlab.com/gotools-land/gtl/commit/0f97d072e689c633780390efa39040ec3bde19a2))
* **dir-executor:** rename parameter variable ([1268d9f](https://gitlab.com/gotools-land/gtl/commit/1268d9fa0719c613cc1630a6f721d823b8b5d120))
* **dir:** added CopyFile, CopyFileWithPerm and CopyDirFunc ([38f163d](https://gitlab.com/gotools-land/gtl/commit/38f163dda61275db28bc71bcbad1b816f6b36d8d))
* **dir:** remove useless func and add alias for exec function ([02bf5f0](https://gitlab.com/gotools-land/gtl/commit/02bf5f03ed6d3439348cbfbcf87fa3b4994988f1))
* **exithandler:** add coverage and use common functions ([a851462](https://gitlab.com/gotools-land/gtl/commit/a8514622e4ff81cecdfaf3c0ceaad668a3448e73))
* **filesystem:** add Exists godoc ([5fd7ed3](https://gitlab.com/gotools-land/gtl/commit/5fd7ed34500b2c1cec985a5f71517d48d72980fe))
* **filesystem:** edit tests to avoid testdata directory ([51efcb3](https://gitlab.com/gotools-land/gtl/commit/51efcb3f4a86263fb2432e60e661310c10534dff))
* go mod tidy ([c6aece6](https://gitlab.com/gotools-land/gtl/commit/c6aece68e833b581f07eabccae5e0e300fadc81d))
* go tidying ([f43a50d](https://gitlab.com/gotools-land/gtl/commit/f43a50d7e5f69b94cd26e559310746906372a2ec))
* **go.mod:** tidying ([44470c9](https://gitlab.com/gotools-land/gtl/commit/44470c9cb04b074c482bd42dfe3b22710cb485b0))
* **go.mod:** upgrade dependencies ([11c673b](https://gitlab.com/gotools-land/gtl/commit/11c673b98251ca8e5b0f0626036812065bfa7a6d))
* **go:** upgrade dependencies ([d765045](https://gitlab.com/gotools-land/gtl/commit/d765045790be5725e822ff0523fa8a9dfcfd7003))
* **go:** upgrade to 1.21 ([456e77d](https://gitlab.com/gotools-land/gtl/commit/456e77de537e0b8e3bc778c66d12e6bb93b6cf3f))
* **go:** upgrade to go1.22 ([3bfd07e](https://gitlab.com/gotools-land/gtl/commit/3bfd07ee2197845cd776e99c8fba277fe6e05f44))
* **license:** add ([fd3741d](https://gitlab.com/gotools-land/gtl/commit/fd3741d0d983dde8c0dbba187f19c97ee5bf076b))
* **lint:** fix lint issues ([32c1a46](https://gitlab.com/gotools-land/gtl/commit/32c1a465e48697a4537c663858222e6251f41b0a))
* **makefile:** remove newlines ([473acf4](https://gitlab.com/gotools-land/gtl/commit/473acf45af3c4c7d29e08012ddeb4abd916c9416))
* **Makefile:** update test-cover command to cover all packages ([0cf8070](https://gitlab.com/gotools-land/gtl/commit/0cf807032237267dd8ba853d642f91ff4b2c89ef))
* regenerate code layout and fix linting issues ([9ce15ce](https://gitlab.com/gotools-land/gtl/commit/9ce15cee6175937d125d35fb42b654b86b0faaa0))
* regenerate files with craft ([527bc22](https://gitlab.com/gotools-land/gtl/commit/527bc220c891dd95e3cdafc02fa2bd6b0aa756e8))
* regenerate layout ([53fffc5](https://gitlab.com/gotools-land/gtl/commit/53fffc54f9280d2cbae277cdefbff63b6f2fdc03))
* regenerate project layout ([5d3fa3f](https://gitlab.com/gotools-land/gtl/commit/5d3fa3f4e97fdec2b1f092d5f2466d5f6b4a1722))
* regenerate project layout ([61e2b6e](https://gitlab.com/gotools-land/gtl/commit/61e2b6e9fe59534af8113bdb0c2c6040b34aa045))
* **release:** v1.0.0 [skip ci] ([1fc3fd7](https://gitlab.com/gotools-land/gtl/commit/1fc3fd7ec57290ec093d47abe9b557a6ebbc899d))
* **release:** v2.0.0 [skip ci] ([d3709a7](https://gitlab.com/gotools-land/gtl/commit/d3709a706d3814f42101b8c1d2bcc3ecf42e6ea9))
* **release:** v2.0.0 [skip ci] ([f138a86](https://gitlab.com/gotools-land/gtl/commit/f138a8689a28d0b922e62750dbf66bc63528987a))
* remove convert package in favor of samber/lo functions ([f0cb2b1](https://gitlab.com/gotools-land/gtl/commit/f0cb2b1949ed1ca09de7452da91193e853f58f3f))
* remove golanci-lint gomnd linter ([378634d](https://gitlab.com/gotools-land/gtl/commit/378634df86d0d386a27972de9d8c733b3c2097db))
* review overall package code ([4d59f56](https://gitlab.com/gotools-land/gtl/commit/4d59f56388c93609e1b233f6400fb8888363ee5f))
* **taskhandler:** order functions by name ([e60ea3b](https://gitlab.com/gotools-land/gtl/commit/e60ea3b07a0c872083278b1d8b596d5eaa2c704e))
* tidying ([7d3b275](https://gitlab.com/gotools-land/gtl/commit/7d3b27513ae6ddf86f49a1b327eda85840f92d84))
* tidying ([e58a497](https://gitlab.com/gotools-land/gtl/commit/e58a497d28a3cd55a29c591f0e9c88c777e8285d))
* update tests for sonar ([3cf57c4](https://gitlab.com/gotools-land/gtl/commit/3cf57c4a94263f596f23ee543af7418cd7d2318d))
* update tests for sonar ([f0b08a8](https://gitlab.com/gotools-land/gtl/commit/f0b08a83f298476bce33494fd32e5213ad360d08))
* **validator:** moved global validator to its package ([24a080e](https://gitlab.com/gotools-land/gtl/commit/24a080e2a6febc6be2005fc72afe09c85810aeb1))
* **validator:** remove useless package ([3f62bac](https://gitlab.com/gotools-land/gtl/commit/3f62bac5726696db7bf5e57cc3fe73cdfc00d99f))
* **yamlmerge:** add ctx to input parameters ([5a8b6e0](https://gitlab.com/gotools-land/gtl/commit/5a8b6e06bf81af1af97ea94801b26f6df7d74702))
* **yamlmerge:** don't preallocate slices ([961160f](https://gitlab.com/gotools-land/gtl/commit/961160fe07bccd94c9126ddd1ee43d55a683f3bf))


### Code Refactoring

* **cache:** rework memory cache with sync.Map ([4694775](https://gitlab.com/gotools-land/gtl/commit/4694775637dd41d5899212b6a43a2c0810aa43aa))
* **constants:** clean up with go:build comments ([e46a5da](https://gitlab.com/gotools-land/gtl/commit/e46a5da9d9e1ed9d609e0c7b147e177907132764))
* **httpcache:** remove useless code and use open source utils ([014fe6e](https://gitlab.com/gotools-land/gtl/commit/014fe6e18692f0a2badc9bfebdbb8ca7551fb3e1))
* make cache behavior more similar between redis and in memory ([4c13ba6](https://gitlab.com/gotools-land/gtl/commit/4c13ba6f2b82d816ae80e4a874df12e1c57b50e1))
* **taskhandler:** moved builder code into taskhandler file ([4faf9b6](https://gitlab.com/gotools-land/gtl/commit/4faf9b649005fbc017f5d3cd57f46376fb25a2d5))
* **taskhandler:** rework to use interface and fix Push bug on channel closed ([e0740da](https://gitlab.com/gotools-land/gtl/commit/e0740da6dad40cd88d8a9b85df38849ef6a2b035))
* use string as cache key instead of uuid.UUID ([0c6722a](https://gitlab.com/gotools-land/gtl/commit/0c6722a95e8707b49373146cf4a49f6cb09f31f5))
